FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get --yes upgrade
RUN apt-get install --yes --quiet --no-install-recommends \
	build-essential \
	git \
	meson \
	cmake \
	cppcheck

RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*

COPY FMLtest /FMLtest
COPY cfml /cfml

RUN true \
	&& meson setup /tmp/build cfml --buildtype release \
	&& meson compile -C /tmp/build \
	&& install -Dm755 /tmp/build/fml cfml/fml \
	&& rm -rf /tmp/build

RUN rm -rf /tmp/* /var/tmp/*
